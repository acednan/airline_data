
import re
import dateutil.parser
"""
Analyzing airline reviews from https://www.airlinequality.com/airline-reviews/germanwings/page/14/
"""

def getcustomer(mappings, index):
	customer = {}
	for qualifier in mappings:
		# print(qualifier)
		# print(mappings[qualifier])
		try:
			customer[qualifier] = mappings[qualifier][index]
		except KeyError:
			pass
	return customer

def countqualifier(mappings, qualifier):
	types = {}
	for key,value in mappings[qualifier].items():
		try:
			types[value] = types[value] + 1
		except KeyError:
			types[value] = 1
	return types

def avgrating(mappings):
	types = countqualifier(mappings, 'Rating')
	total_ratings = 0
	sum_ratings = 0
	for key,value in types.items():
		sum_ratings = sum_ratings + (key*value)
		total_ratings = total_ratings + value
	avg = sum_ratings/total_ratings
	return round(avg,2)

def yearofreviews(mappings):
	types = {}
	for key,value in mappings['Date'].items():
		try:
			types[value.year] = types[value.year] + 1
		except KeyError:
			types[value.year] = 1
	return types

def yearvsrecommended(mappings):
	types = {}
	for key,value in mappings['Date'].items():
		try:
			types[value.year].append(mappings['Recommended'][key])
		except KeyError:
			types[value.year] = [mappings['Recommended'][key]]
	final_counts = {}
	for key,value in types.items():
		final_counts[key] = dict((x,value.count(x)) for x in set(value))
	sorted_counts = {}
	for key in sorted(final_counts):
		sorted_counts[key] = final_counts[key]
	return sorted_counts

	





lines = open('4U_Reviews.txt', encoding = "ISO-8859-1").read().splitlines()
qualifiers = ['Type Of Traveller', 'Cabin Flown', 'Route', 'Date Flown', 'Seat Comfort', 'Cabin Staff Service', 
'Ground Service', 'Value For Money', 'Recommended', 'Aircraft', 'Food & Beverages', 'Inflight Entertainment', 'Germanwings customer review']
date_regx = re.compile(r'[0-9]+[a-zA-Z]{2}\s[a-zA-Z]+\s[0-9]+')
name_regx = re.compile(r'^[a-zA-Z]+\s[a-zA-Z]+')
country_regx = re.compile(r'\((.+)\)')
mappings = {}
for qualifier in qualifiers:
	mappings[qualifier] = {}
mappings['Review'] = {}
mappings['Rating'] = {}
mappings['Date'] = {}
mappings['Name'] = {}
mappings['Country'] = {}

count = 1

for line in lines:
	if line == '':
		count += 1
		continue
	for qualifier in qualifiers:
		reviewdate = date_regx.findall(line)
		if line.startswith(qualifier):
			try:
			    mappings[qualifier][count] = line.split('\t')[1]
			    break
			except IndexError:
				pass
			continue
		elif line.isdigit():
			mappings['Rating'][count] = int(line)
			break
		elif len(line) > 50:
			mappings['Review'][count] = line
			break
		elif len(reviewdate) == 1:
			mappings['Date'][count] = dateutil.parser.parse(reviewdate[0])
			name = name_regx.findall(line)
			country = country_regx.findall(line)
			if len(name) == 1:
				mappings['Name'][count] = name[0]
			if len(country) == 1:
				mappings['Country'][count] = country[0]



print(mappings)
print(len(mappings['Rating']))
print("length of reviews:" ,len(mappings['Review']))
print(count)
print(mappings['Rating'][128])
print(mappings['Review'][128])
print('-----------------------')
print(getcustomer(mappings,126))
print('---------------------')
print(countqualifier(mappings,'Recommended'))
print('Rating')
print(countqualifier(mappings, 'Rating'))
print('Average Rating')
print(avgrating(mappings))
print('Year of Reviews')
print(yearofreviews(mappings))
print('yearvsrecommended')
print(yearvsrecommended(mappings))

